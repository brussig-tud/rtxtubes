
//////
//
// Includes
//

// Implemented header
#include "hspline.h"



//////
//
// Explicit template instantiations
//

// Only float and double variants are intended
template struct HermiteSpline<float>;
template struct HermiteSpline<double>;
