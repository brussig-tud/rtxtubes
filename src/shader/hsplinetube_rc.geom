#version 460
#extension GL_KHR_vulkan_glsl : enable


////
// Shader config and constants

// Geometry shader configuration
layout (lines) in;
layout (triangle_strip, max_vertices = 14) out;

// Uniforms
layout(binding = 0) uniform CGVuTransforms {
	mat4 modelview;
	mat4 proj;
	mat4 normalMat;
} cgvu_trans;

// Inputs
layout(location = 0) in vec4 in_tan[];
layout(location = 1) in vec4 in_col[];
layout(location = 2) in vec4 in_dcol[];
layout(location = 3) in vec2 in_rad[];

// Outputs
layout(location = 0) out vec3 v_position;
layout(location = 1) out vec4 v_color;

// Includes
#include "hsplinetube_common.glsl"

// Macros
#define _C [i]

// Globals
const float _posInf = 3e+37, _negInf = -3e+37;
#define BBOX_LEFT_DN_FRNT bbox[0][0]
#define BBOX_LEFT_UP_FRNT bbox[0][1]
#define BBOX_RGHT_DN_FRNT bbox[0][2]
#define BBOX_RGHT_UP_FRNT bbox[0][3]
#define BBOX_RGHT_DN_BACK bbox[1][0]
#define BBOX_RGHT_UP_BACK bbox[1][1]
#define BBOX_LEFT_DN_BACK bbox[1][2]
#define BBOX_LEFT_UP_BACK bbox[1][3]
const mat4 unitCube[2] = {
	{vec4(0, 0, 0, 1),
	 vec4(0, 1, 0, 1),
	 vec4(1, 0, 0, 1),
	 vec4(1, 1, 0, 1)},
	{vec4(1, 0, 1, 1),
	 vec4(1, 1, 1, 1),
	 vec4(0, 0, 1, 1),
	 vec4(0, 1, 1, 1)}
};


////
// Functions

// Finds the per-component minima from the given extrema and interval endpoints of a
// given curve
vec3 getCurveMinimum (in mat3 extrema, in mat2x3 xend, in mat4x3 curve)
{
	const mat3x4 curveT = transpose(curve);
	vec3 pmin;
	for (int i=0; i<3; i++)
	{
		float qm1 = extrema[0]_C < 1 || isOutside01(extrema[1]_C) ?
		            	_posInf : evalBezier((curveT)_C, extrema[1]_C),
		      qm2 = extrema[0]_C < 2 || isOutside01(extrema[2]_C) ?
		            	_posInf : evalBezier((curveT)_C, extrema[2]_C);
		(pmin)_C = min(xend[0]_C, min(min(qm1, qm2), xend[1]_C));
	}
	return pmin;
}

// Finds the per-component maxima from the given extrema and interval endpoints of a
// given curve
vec3 getCurveMaximum (in mat3 extrema, in mat2x3 xend, in mat4x3 curve)
{
	const mat3x4 curveT = transpose(curve);
	vec3 pmax;
	for (int i=0; i<3; i++)
	{
		float qm1 = extrema[0]_C < 1 || isOutside01(extrema[1]_C) ?
		            	_negInf : evalBezier((curveT)_C, extrema[1]_C),
		      qm2 = extrema[0]_C < 2 || isOutside01(extrema[2]_C) ?
		            	_negInf : evalBezier((curveT)_C, extrema[2]_C);
		(pmax)_C = max(xend[0]_C, max(max(qm1, qm2), xend[1]_C));
	}
	return pmax;
}

// Shader entry point
void main()
{
	// Determine principle sideways direction
	const vec3 p0 = gl_in[0].gl_Position.xyz/gl_in[0].gl_Position.w,
	           p1 = gl_in[1].gl_Position.xyz/gl_in[1].gl_Position.w,
	           diff = p1 - p0, fore = normalize(diff),
	           sideways0 = in_tan[0].xyz - fore*dot(fore, in_tan[0].xyz),
	           sideways1 = in_tan[1].xyz - fore*dot(fore, in_tan[1].xyz);
	const float sideways0_sqrLen = dot(sideways0,sideways0),
	            sideways1_sqrLen = dot(sideways1,sideways1);
	const bool choseTan = sideways0_sqrLen > 0 || sideways1_sqrLen > 0,
	           choseSW0 = sideways0_sqrLen >= sideways1_sqrLen;
	const vec3 side = normalize(
	           	choseTan ? (choseSW0 ? sideways0 : sideways1) : cross(fore, fore.zyx)
	           ),
	           up = cross(fore, side);

	// Construct rotation matrix for oriented space
	const mat4 R = {
		vec4(side.x, up.x, fore.x, 0), vec4(side.y, up.y, fore.y, 0),
		vec4(side.z, up.z, fore.z, 0), vec4(0, 0, 0, 1)
	};

	// Convert to Bezier form for efficient evaluation and transform curve to oriented
	// space at the same time
	const mat4x3 p = mat3(R) * toBezier(p0, in_tan[0].xyz, p1, in_tan[1].xyz);
	const vec4 r = toBezier(in_rad[0].r, in_rad[0].g, in_rad[1].r, in_rad[1].g);

	// Differentiate component min/max curves
	const mat4x3 r4x3 = mat4x3(vec3(r[0]), vec3(r[1]), vec3(r[2]), vec3(r[3]));
	const mat4x3 qminus = p - r4x3, qplus = p + r4x3;
	const mat3 dqminus = toMonomial(deriveBezier(qminus)),
	           dqplus = toMonomial(deriveBezier(qplus));

	// Determine extrema 
	const mat3 qmnExtr = solveQuadratic(dqminus),
	           qplExtr = solveQuadratic(dqplus);
	const mat2x3 qmin = mat2x3(evalBezier(qminus, 0.f), evalBezier(qminus, 1.f)),
	             qmax = mat2x3(evalBezier(qplus, 0.f), evalBezier(qplus, 1.f));
	const vec3 pmin = getCurveMinimum(qmnExtr, qmin, qminus),
	           pmax = getCurveMaximum(qplExtr, qmax, qplus);

	// Construct scaling/translation matrix for final oriented bounding box
	const vec3 ext = pmax - pmin;
	const mat4 ST = {
		vec4(ext.x, 0, 0, 0), vec4(0, ext.y, 0, 0), vec4(0, 0, ext.z, 0),
		vec4(pmin.x, pmin.y, pmin.z, 1)
	},
	M = transpose(R) * ST;

	// Bounding box vertices
	const mat4 bbox[2] = {
		cgvu_trans.modelview * M * unitCube[0], cgvu_trans.modelview * M * unitCube[1]
	};

	// Emit box strip
	// - 14 strip vertices, stripification according to Evans et al.
	v_position = BBOX_LEFT_UP_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_RGHT_UP_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_LEFT_DN_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_RGHT_DN_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_RGHT_DN_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_RGHT_UP_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_RGHT_UP_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_LEFT_UP_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_LEFT_UP_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_LEFT_DN_FRNT.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_FRNT;
	v_color = in_col[0];
	EmitVertex();
	v_position = BBOX_LEFT_DN_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_DN_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_RGHT_DN_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_DN_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_LEFT_UP_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_LEFT_UP_BACK;
	v_color = in_col[1];
	EmitVertex();
	v_position = BBOX_RGHT_UP_BACK.xyz;
	gl_Position = cgvu_trans.proj * BBOX_RGHT_UP_BACK;
	v_color = in_col[1];
	EmitVertex();
	// - finalize
	EndPrimitive();
}
