
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <exception>
#include <stdexcept>

// GLFW library (including Vulkan)
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

// GLM library
#define GLM_FORCE_SWIZZLE 1
#include <glm/gtc/matrix_transform.hpp>

// ImGUI library
// - ToDo: abstract away behind framework functionality to eliminate this include
#include <imgui.h>

// CGVu library
#include <cgvu/util/utils.h>
#include <cgvu/application.h>
#include <cgvu/vk/buffer.h>
#include <cgvu/vk/texture.h>
#include <cgvu/vk/shaderprog.h>
#include <cgvu/vk/vulkan_internals.h>

// Local includes
//#include "curveutils.h"
#include "hspline.h"
#include "bezdat.h"



//////
//
// Setup constants
//

int viewportWidth = 1152;
int viewportHeight = 864;



//////
//
// Module-private structs
//

// Encapsulates the Vulkan RTX entry points
struct RTXfuncs
{
	// Convenience type for map of Context -> function table
	typedef std::map<const cgvu::Context*, RTXfuncs> Map;

	// List of RTX functions used by the app
	PFN_vkCreateAccelerationStructureNV vkCreateAccelerationStructureNV;
	PFN_vkGetAccelerationStructureMemoryRequirementsNV
		vkGetAccelerationStructureMemoryRequirementsNV;
	PFN_vkBindAccelerationStructureMemoryNV vkBindAccelerationStructureMemoryNV;
	PFN_vkGetAccelerationStructureHandleNV vkGetAccelerationStructureHandleNV;

	// Default constuctor for use with STL containers
	RTXfuncs() {}

	// Actual-use constructor
	RTXfuncs(const cgvu::Context& ctx)
	{
		vkCreateAccelerationStructureNV =
			(PFN_vkCreateAccelerationStructureNV)vkGetDeviceProcAddr(
				ctx.handle(), "vkCreateAccelerationStructureNV"
			);
		vkGetAccelerationStructureMemoryRequirementsNV =
			(PFN_vkGetAccelerationStructureMemoryRequirementsNV)vkGetDeviceProcAddr(
				ctx.handle(), "vkGetAccelerationStructureMemoryRequirementsNV"
			);
		vkBindAccelerationStructureMemoryNV =
			(PFN_vkBindAccelerationStructureMemoryNV)vkGetDeviceProcAddr(
				ctx.handle(), "vkBindAccelerationStructureMemoryNV"
			);
		vkGetAccelerationStructureHandleNV =
			(PFN_vkGetAccelerationStructureHandleNV)vkGetDeviceProcAddr(
				ctx.handle(), "vkGetAccelerationStructureHandleNV"
			);
	}

	// Obtain an RTX function table for a given context
	static RTXfuncs& obtain (RTXfuncs::Map &map, const cgvu::Context &ctx)
	{
		auto it = map.find(&ctx);
		if (it != map.end())
			return it->second;
		return map.emplace(&ctx, ctx).first->second;
	}
};

// Encapsulates data for an RTX acceleration data structure
struct AccelDS
{
	VkDeviceMemory                mem;
	VkAccelerationStructureInfoNV ci;
	VkAccelerationStructureNV     handle;
	uint64_t                      devHandle;
};



//////
//
// Module-private classes
//

// Application
class RTXtubesApp : public cgvu::ApplicationStub<float>
{

public:

	////
	// Exported types

	// Hermite Node vertex type
	struct NodeVertex {
		Vec4 pos, tan, col, dcol;
		Vec2 rad;

		inline static cgvu::BufferBindingDescs& bindingDescs(void)
		{
			static cgvu::BufferBindingDescs bd = { { 0, sizeof(NodeVertex), false } };
			return bd;
		}
		inline static cgvu::AttribDescs& attributeDescs(void)
		{
			static cgvu::AttribDescs ad = {
				{ 0, 0, offsetof(NodeVertex, pos), cgvu::AFmt::VEC4_FLT32 },
				{ 1, 0, offsetof(NodeVertex, tan), cgvu::AFmt::VEC4_FLT32 },
				{ 2, 0, offsetof(NodeVertex, col), cgvu::AFmt::VEC4_FLT32 },
				{ 3, 0, offsetof(NodeVertex, dcol), cgvu::AFmt::VEC4_FLT32 },
				{ 4, 0, offsetof(NodeVertex, rad), cgvu::AFmt::VEC2_FLT32 }
			};
			return ad;
		}
	};

	// Uniform structs
	struct CGVuTransforms {
		Mat4 modelview;
		Mat4 proj;
		Mat4 normalMat;
	};
	struct CGVuMaterial {
		Vec4 albedo;
		float specularity;
		float shininess;
	};
	struct CGVuGlobalLight {
		Vec3 pos;   // in eye space
		alignas(16) Vec3 color;
	};


protected:

	////
	// Data members

	// Low-level Vulkan objects
	// - function tables
	RTXfuncs::Map RTXfuncTable;
	// - acceleration data structures
	VkGeometryNV testGeo;
	AccelDS accelDS;
	// - scratch memory
	// - raytracing

	// High-level Vulkan objects
	// - command buffers
	cgvu::CommandBuffer cmd;

	// Dataset
	// - geometry
	std::vector<NodeVertex> vertices;
	std::vector<unsigned> indices;
	// - buffers
	cgvu::StorageBuffer vertexBuf, indexBuf;

	// The test shader
	cgvu::ShaderProgram sh;
	cgvu::UniformBuffer shUniforms;
	cgvu::TypedUniform<CGVuTransforms> shTrans;
	cgvu::TypedUniform<CGVuMaterial> shMat;
	cgvu::TypedUniform<CGVuGlobalLight> shLight;

	// The test pipeline
	cgvu::Pipeline pl;

	// State
	Real angle=0;


public:

	////
	// Object construction / destruction

	// Virtual base destructor. Causes vtable creation.
	RTXtubesApp() : sh(cgvu::SHADER_SRC_SPK, "shader/hsplinetube_rc.spk")
	{
		configureMainWindow(viewportWidth, viewportHeight);
		#ifdef _DEBUG
			setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::INFO);
			setVulkanLogLevel(cgvu::Dbg::VALIDATION, cgvu::log::WARNING);
		#else
			setVulkanLogLevel(cgvu::Dbg::LOADER_DRIVER, cgvu::log::WARNING);
		#endif
	}


	////
	// Interface: cgvu::Application

	// App name
	virtual std::string getName (void)
	{
		return "RTXtubes";
	}

	// Instance layers we require
	virtual void declareRequiredInstanceLayers (std::vector<const char*> *out) const
	{
	}

	// Instance extensions we require
	virtual void declareRequiredInstanceExtensions (std::set<std::string> *out) const
	{
	}

	// Device extensions we require
	virtual void declareRequiredDeviceExtensions (
		std::map<std::string, cgvu::Vulkan::DeviceExtensionInfo> *out
	) const
	{
		static VkPhysicalDeviceAccelerationStructureFeaturesKHR features_RTXAccelDS = {
			// sType
			VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR,
			// pNext (ignored, will be set up by cgvu internally)
			nullptr,
			// accelerationStructure;
			true,
			// accelerationStructureCaptureReplay
			false,
			// accelerationStructureIndirectBuild
			false,
			// accelerationStructureHostCommands
			false,
			// descriptorBindingAccelerationStructureUpdateAfterBind
			false
		};
		static VkPhysicalDeviceRayTracingPipelineFeaturesKHR features_RTXpipeline = {
			// sType
			VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR,
			// pNext (ignored, will be set up by cgvu internally)
			nullptr,
			// rayTracingPipeline
			true,
			// rayTracingPipelineShaderGroupHandleCaptureReplay
			false,
			// rayTracingPipelineShaderGroupHandleCaptureReplayMixed
			false,
			// rayTracingPipelineTraceRaysIndirect,
			false,
			// rayTraversalPrimitiveCulling
			true
		};
		out->emplace(
			VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME,
			cgvu::Vulkan::DeviceExtensionInfo{0, &features_RTXAccelDS}
		);
		out->emplace(
			VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME,
			cgvu::Vulkan::DeviceExtensionInfo{0, &features_RTXpipeline}
		);
	}

	// Post-constructor init
	virtual void mainInit (cgvu::Context &ctx)
	{
		// Load dataset
		auto &bezdat = BezdatHandler<Real>::obtainRef();
		const std::string dataFilename(
			"../data/Fibers.bezdat"
		); // Large_seed128-n1250-t2000-v0.25 | Huge_seed128-n7000-t6333-v0.015625
		std::ifstream dataFile(dataFilename);
		HermiteSpline<Real> splines = bezdat.read(dataFile);
		std::cout << "Dataset \""<<cgvu::util::nameWithoutPath(dataFilename)<<"\":"
		          << std::endl << "  - tubes: " << splines.curveCount << std::endl
		          << "  - segments: " << splines.segments.size() << std::endl
		          << "  - nodes: " << splines.positions.size() << std::endl<<std::endl;

		// Raycast shader
		sh.build(ctx, {
			{0, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::GEOMETRY},
			{1, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
			{2, cgvu::UFrm::INTEGRAL, 1, cgvu::SH::FRAGMENT},
		});
		shUniforms = cgvu::UniformBuffer(ctx);
		shTrans = shUniforms.insert<CGVuTransforms>();
		shMat = shUniforms.insert<CGVuMaterial>();
		shLight = shUniforms.insert<CGVuGlobalLight>();
		shUniforms.build();
		sh.updateDescriptorSets({
			cgvu::UniformUpdate(0, shTrans),
			cgvu::UniformUpdate(1, shMat),
			cgvu::UniformUpdate(2, shLight)
		});

		// Setup dataset for rendering
		// - material
		shMat.initialize({/*albedo*/{1, 1, 1, 1}, /*specularity*/0.5, /*shininess*/96});
		// - nodes
		vertices.reserve(splines.positions.size());
		for (size_t i=0; i<vertices.capacity(); i++)
			vertices.emplace_back(NodeVertex{
				splines.positions[i].val, splines.positions[i].der,
				splines.colors[i].val, splines.colors[i].der,
				Vec2{splines.radii[i].val, splines.radii[i].der}
			});
		// - segments
		indices.reserve(splines.segments.size()*2);
		for (const auto &seg : splines.segments)
		{
			indices.push_back(seg.n0);
			indices.push_back(seg.n1);
		}
		//// +DEBUG (GLSL playground) ////
		/*testCurveUtils();
		{
		typedef Mat3 mat3;
		typedef Mat4 mat4;
		typedef Vec3 vec3;
		typedef Vec4 vec4;
		typedef glm::mat<4,3,Real> mat4x3;
		typedef glm::mat<3,4,Real> mat3x4;
		typedef glm::mat<2,3,Real> mat2x3;
		const mat4 unitCube[2] = {
			{vec4(0, 0, 0, 1),
			 vec4(0, 1, 0, 1),
			 vec4(1, 0, 0, 1),
			 vec4(1, 1, 0, 1)},
			{vec4(1, 0, 1, 1),
			 vec4(1, 1, 1, 1),
			 vec4(0, 0, 1, 1),
			 vec4(0, 1, 1, 1)}
		};
		#define _C [i]
		auto getCurveMinimum = [] (
			const mat3 &extrema, const mat2x3 &xend, const mat4x3 &curve
		) -> vec3 {
			const mat3x4 curveT = transpose(curve);
			vec3 pmin;
			for (int i=0; i<3; i++)
			{
				Real qm1 = extrema[0]_C < 1 || isOutside01(extrema[1]_C) ?
				           	_posInf<Real> : evalBezier((curveT)_C, extrema[1]_C),
				     qm2 = extrema[0]_C < 2 || isOutside01(extrema[2]_C) ?
				           	_posInf<Real> : evalBezier((curveT)_C, extrema[2]_C);
				(pmin)_C = glm::min(xend[0]_C, glm::min(glm::min(qm1, qm2), xend[1]_C));
			}
			return pmin;
		};
		auto getCurveMaximum = [] (
			const mat3 &extrema, const mat2x3 &xend, const mat4x3 &curve
		) -> vec3 {
			const mat3x4 curveT = transpose(curve);
			vec3 pmax;
			for (int i=0; i<3; i++)
			{
				Real qm1 = extrema[0]_C < 1 || isOutside01(extrema[1]_C) ?
				           	_negInf<Real> : evalBezier((curveT)_C, extrema[1]_C),
				     qm2 = extrema[0]_C < 2 || isOutside01(extrema[2]_C) ?
				           	_negInf<Real> : evalBezier((curveT)_C, extrema[2]_C);
				(pmax)_C = glm::max(xend[0]_C, glm::max(glm::max(qm1, qm2), xend[1]_C));
			}
			return pmax;
		};
		for (const auto &seg : splines.segments)
		{
			// Convenience shortcuts for eye space positions
			struct Scgvu_trans {
				mat4 modelview = mat4(1);
			} cgvu_trans;
			struct Sgl_in {
				vec4 gl_Position;
			} gl_in[2];
			struct Sin_rad {
				Real r, g;
			} in_rad[2];
			vec4 in_tan[2];
			gl_in[0].gl_Position = splines.positions[seg.n0].val;
			gl_in[1].gl_Position = splines.positions[seg.n1].val;
			in_tan[0] = splines.positions[seg.n0].der;
			in_tan[1] = splines.positions[seg.n1].der;
			in_rad[0].r = splines.radii[seg.n0].val;
			in_rad[0].g = splines.radii[seg.n0].der;
			in_rad[1].r = splines.radii[seg.n1].val;
			in_rad[1].g = splines.radii[seg.n1].der;

			// Determine principle sideways direction
			const vec3 p0 = gl_in[0].gl_Position.xyz()/gl_in[0].gl_Position.w,
			           p1 = gl_in[1].gl_Position.xyz()/gl_in[1].gl_Position.w,
			           diff = p1 - p0, fore = normalize(diff),
			           sideways0 = in_tan[0].xyz() - fore*dot(fore, in_tan[0].xyz()),
			           sideways1 = in_tan[1].xyz() - fore*dot(fore, in_tan[1].xyz());
			const float sideways0_sqrLen = dot(sideways0,sideways0),
			            sideways1_sqrLen = dot(sideways1,sideways1);
			const bool choseTan = sideways0_sqrLen > 0 || sideways1_sqrLen > 0,
			           choseSW0 = sideways0_sqrLen >= sideways1_sqrLen;
			const vec3 side = normalize(
			           	choseTan ? (choseSW0 ? sideways0 : sideways1) : cross(fore, fore.zyx())
			           ),
			           up = cross(fore, side);

			// Construct rotation matrix for oriented space
			const mat4 R = {
				vec4(side.x, up.x, fore.x, 0), vec4(side.y, up.y, fore.y, 0),
				vec4(side.z, up.z, fore.z, 0), vec4(0, 0, 0, 1)
			};

			// Convert to Bezier form for efficient evaluation and transform curve to oriented
			// space at the same time
			const mat4x3 p = mat3(R) * toBezier(p0, in_tan[0].xyz(), p1, in_tan[1].xyz());
			const vec4 r = toBezier(in_rad[0].r, in_rad[0].g, in_rad[1].r, in_rad[1].g);

			// Differentiate component min/max curves
			const mat4x3 r4x3 = mat4x3(vec3(r[0]), vec3(r[1]), vec3(r[2]), vec3(r[3]));
			const mat4x3 qminus = p - r4x3, qplus = p + r4x3;
			const mat3 dqminus = toMonomial(deriveBezier(qminus)),
			           dqplus = toMonomial(deriveBezier(qplus));

			// Determine extrema 
			const mat3 qmnExtr = solveQuadratic(dqminus),
			           qplExtr = solveQuadratic(dqplus);
			const mat2x3 qmin = mat2x3(evalBezier(qminus, 0.f), evalBezier(qminus, 1.f)),
			             qmax = mat2x3(evalBezier(qplus, 0.f), evalBezier(qplus, 1.f));
			const vec3 pmin = getCurveMinimum(qmnExtr, qmin, qminus),
			           pmax = getCurveMaximum(qplExtr, qmax, qplus);

			// Construct scaling/translation matrix for final oriented bounding box
			const vec3 ext = pmax - pmin;
			const mat4 ST = {
				vec4(ext.x, 0, 0, 0), vec4(0, ext.y, 0, 0), vec4(0, 0, ext.z, 0),
				vec4(pmin.x, pmin.y, pmin.z, 1)
			},
			M = transpose(R) * ST;

			// Bounding box vertices
			const mat4 bbox[2] = {
				cgvu_trans.modelview * M * unitCube[0], cgvu_trans.modelview * M * unitCube[1]
			};

			// Validate the result
			for (unsigned i=0; i<3; i++)
			{
				assert(   qmnExtr[0][i] == 0 || qmnExtr[0][i] == Real(1)
				       || qmnExtr[0][i] == Real(2));
				if (qmnExtr[0][i] == Real(2))
					assert(!(   glm::isnan(qmnExtr[1][i]) || glm::isinf(qmnExtr[1][i])
					         || glm::isnan(qmnExtr[2][i]) || glm::isinf(qmnExtr[2][i])));
				else if (qmnExtr[0][i] == Real(1))
					assert(!(glm::isnan(qmnExtr[1][i]) || glm::isinf(qmnExtr[1][i])));

				assert(   qplExtr[0][i] == 0 || qplExtr[0][i] == Real(1)
				       || qplExtr[0][i] == Real(2));
				if (qplExtr[0][i] == Real(2))
					assert(!(   glm::isnan(qplExtr[1][i]) || glm::isinf(qplExtr[1][i])
					         || glm::isnan(qplExtr[2][i]) || glm::isinf(qplExtr[2][i])));
				else if (qplExtr[0][i] == Real(1))
					assert(!(glm::isnan(qplExtr[1][i]) || glm::isinf(qplExtr[1][i])));
			}

			// Convenient line for setting a final breakpoint
			std::cout.flush();
		}
		}*/
		//// -DEBUG ////

		// Test buffers
		vertexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::VERTEX);
		vertexBuf.upload(vertices);
		indexBuf = cgvu::StorageBuffer(ctx, cgvu::SBT::INDEX);
		indexBuf.upload(indices);

		// Test pipeline
		pl = ctx.createPipeline(sh);
		pl.setInputBindings<NodeVertex>(cgvu::PT::LINE_LIST);
		pl.attachDepthStencilBuffer(mainDepthStencil());
		pl.build();

		// Test command buffers
		// - test render
		cmd = ctx.createCommandBuffer(cgvu::TC::RENDER, false);
		cmd.startRecording();
		cmd.bindPipeline(pl);
		cmd.bindBuffer(NodeVertex::bindingDescs()[0].binding, vertexBuf);
		cmd.bindIndexBuffer(indexBuf);
		cmd.drawIndexed(indices.size(), 1, 0);
		cmd.build();

		// Attach light to camera (by specifying position in eye coordinates)
		shLight.initialize({/*pos*/{0.125, 0.0625, 0}, /*color*/{1, 1, 1}});


		////
		// RTX

		// Vulkan shortcuts
		/*VkDevice dev = ctx.handle();
		const auto &devInfo = *(VulkanDeviceInfo*)ctx.getDeviceInfo();

		// Load functions entry points
		auto &rtx = RTXfuncs::obtain(RTXfuncTable, ctx);

		// Build acceleration data structures - geometry
		// - create geometry descriptor
		testGeo.sType = VK_STRUCTURE_TYPE_GEOMETRY_NV;
		testGeo.pNext = nullptr;
		testGeo.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_NV;
		testGeo.geometry.triangles.sType = VK_STRUCTURE_TYPE_GEOMETRY_TRIANGLES_NV;
		testGeo.geometry.triangles.pNext = nullptr;
		testGeo.geometry.triangles.vertexData = vertexBuf.handle();
		testGeo.geometry.triangles.vertexOffset = 0;
		testGeo.geometry.triangles.vertexCount = (uint32_t)vertices.size();
		testGeo.geometry.triangles.vertexStride = NodeVertex::bindingDescs()[0].stride;
		testGeo.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
		testGeo.geometry.triangles.indexData = indexBuf.handle();
		testGeo.geometry.triangles.indexOffset = 0;
		testGeo.geometry.triangles.indexCount = (uint32_t)indices.size();
		testGeo.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
		testGeo.geometry.triangles.transformData = VK_NULL_HANDLE;
		testGeo.geometry.triangles.transformOffset = 0;
		testGeo.geometry.aabbs = { };
		testGeo.geometry.aabbs.sType = VK_STRUCTURE_TYPE_GEOMETRY_AABB_NV;
		testGeo.flags = VK_GEOMETRY_OPAQUE_BIT_NV;
		// - create Vulkan object
		accelDS.ci.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_INFO_NV;
		accelDS.ci.pNext = nullptr;
		accelDS.ci.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_NV;
		accelDS.ci.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_NV;
		accelDS.ci.geometryCount = 1;
		accelDS.ci.pGeometries = &testGeo;
		accelDS.ci.instanceCount = 0;
		VkAccelerationStructureCreateInfoNV accellDS_ci;
		accellDS_ci.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_NV;
		accellDS_ci.pNext = nullptr;
		accellDS_ci.info = accelDS.ci;
		accellDS_ci.compactedSize = 0;
		VkResult result = rtx.vkCreateAccelerationStructureNV(
			dev, &accellDS_ci, nullptr, &(accelDS.handle)
		);
		if (result != VK_SUCCESS)
			throw cgvu::err::VulkanException(result, CGVU_LOGMSG(
				"RTXtubesApp::mainInit", "Unable to create geometry acceleration data"
				" structure object!"
			));
		// - allocate memory
		VkAccelerationStructureMemoryRequirementsInfoNV memReqsInfo;
		memReqsInfo.sType =
			VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_INFO_NV;
		memReqsInfo.pNext = nullptr;
		memReqsInfo.type = VK_ACCELERATION_STRUCTURE_MEMORY_REQUIREMENTS_TYPE_OBJECT_NV;
		memReqsInfo.accelerationStructure = accelDS.handle;
		VkMemoryRequirements2 memReqs{};
		memReqs.sType = VK_STRUCTURE_TYPE_MEMORY_REQUIREMENTS_2;
		rtx.vkGetAccelerationStructureMemoryRequirementsNV(dev, &memReqsInfo, &memReqs);
		VkMemoryAllocateInfo allocInfo;
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.pNext = nullptr;
		allocInfo.allocationSize = memReqs.memoryRequirements.size;
		allocInfo.memoryTypeIndex = devInfo.findMemoryType(
			memReqs.memoryRequirements, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
		);
		result = vkAllocateMemory(dev, &allocInfo, nullptr, &(accelDS.mem));
		if (result != VK_SUCCESS)
			throw cgvu::err::VulkanException(result, CGVU_LOGMSG(
				"RTXtubesApp::mainInit", "Unable to allocate scratch memory for building"
				" the geometry acceleration data structure!"
			));
		// - bind memory
		VkBindAccelerationStructureMemoryInfoNV bi;
		bi.sType = VK_STRUCTURE_TYPE_BIND_ACCELERATION_STRUCTURE_MEMORY_INFO_NV;
		bi.pNext = nullptr;
		bi.accelerationStructure = accelDS.handle;
		bi.memory = accelDS.mem;
		bi.memoryOffset = 0;
		bi.deviceIndexCount = 0;
		bi.pDeviceIndices = nullptr;
		result = rtx.vkBindAccelerationStructureMemoryNV(dev, 1, &bi);
		if (result != VK_SUCCESS)
			throw cgvu::err::VulkanException(result, CGVU_LOGMSG(
				"RTXtubesApp::mainInit", "Unable to bind scratch memory to geometry"
				" acceleration data structure!"
			));
		result = rtx.vkGetAccelerationStructureHandleNV(
			dev, accelDS.handle, sizeof(accelDS.devHandle), &(accelDS.devHandle)
		);
		if (result != VK_SUCCESS)
			throw cgvu::err::VulkanException(result, CGVU_LOGMSG(
				"RTXtubesApp::mainInit", "Unable to obtain device code handle for"
				" geometry acceleration data structure!"
			));*/
	}

	// GUI drawing
	void drawGUI (cgvu::Context &context)
	{
		// Just the ImGUI demo for now
		ImGui::ShowDemoWindow();
	}

	// Main loop body
	virtual bool mainLoop (cgvu::Context &ctx, const Stats &stats)
	{
		// Update State
		angle = glm::mod<Real>(angle + 5*stats.frameTime, 360);

		// Handle input
		auto &io = ImGui::GetIO();

		// Apply node transformation
		auto &transf = transforms();
		transf.pushModelview();
		transf.modelview() = glm::rotate(
			transf.modelview(), (float)glm::radians(angle), {0, 1, 0}
		);

		// Update uniforms
		auto &trans = shTrans.set();
		trans.modelview = transf.modelview();
		trans.proj = transf.projection();
		trans.normalMat = transf.getNormalMat();

		// Render
		ctx.submitCommands(cmd);

		// Reset our changes to the transformation stack
		transf.popModelview();

		// Shut down on ESC key
		return !(io.WantCaptureKeyboard) && ImGui::IsKeyPressed(GLFW_KEY_ESCAPE, false);
	}
};



//////
//
// Application entry point
//

int main (int argc, char* argv[])
{
	// Create application object
	RTXtubesApp app;

	// Hand off control flow
	return app.run(argc, argv);
}
