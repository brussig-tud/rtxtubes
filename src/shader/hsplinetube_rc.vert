#version 450
#extension GL_KHR_vulkan_glsl : enable


////
// Shader config and constants

// Inputs
layout(location = 0) in vec4 in_pos;
layout(location = 1) in vec4 in_tan;
layout(location = 2) in vec4 in_col;
layout(location = 3) in vec4 in_dcol;
layout(location = 4) in vec2 in_rad;

// Outputs
layout(location = 0) out vec4 out_tan;
layout(location = 1) out vec4 out_col;
layout(location = 2) out vec4 out_dcol;
layout(location = 3) out vec2 out_rad;


////
// Functions

// Shader entry point
void main()
{
	// Pass-through everything
	out_tan = in_tan;
	out_col = in_col;
	out_dcol = in_dcol;
	out_rad = in_rad;
	gl_Position = in_pos;
}
