
//////
//
// Includes
//

// C++ STL
#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <utility>
#include <limits>

// GLM library
#define GLM_FORCE_SWIZZLE 1
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/vector_angle.hpp>

// CGVU includes
#include <cgvu/error.h>
#include <cgvu/os/os.h>
#include <cgvu/util/utils.h>
#include <cgvu/util/regulargrid.h>

// Local includes
#include "hspline.h"

// Implemented header
#include "bezdat.h"



//////
//
// Module-private globals and helper functions
//

// Anonymous namespace begin
namespace {

// Platform-dependent case-insensitive string comparison
inline int strCmpI (const char *l, const char *r)
{
#ifdef WIN32
	return _stricmp(l, r);
#else
	return strcasecmp(l, r);
#endif
}

// Multiple of the machine epsilon that serves as a tolerance to decide whether two node
// position values are similar or not
#define BEZDAT_SIM_TOLERANCE_POS Real(32)

// Multiple of the Euclidean distance unit for use as a tolerance to decide whether two
// node tangents are similar or not
#define BEZDAT_SIM_TOLERANCE_DPOS Real(0.0078125)

// Multiple of the machine epsilon that serves as a tolerance to decide whether two node
// radius values are similar or not
#define BEZDAT_SIM_TOLERANCE_RAD Real(32)

// Multiple of the Euclidean distance unit for use as a tolerance to decide whether two
// node radius derivatives are similar or not
#define BEZDAT_SIM_TOLERANCE_DRAD Real(0.0078125)

// Multiple of the machine epsilon that serves as a tolerance to decide whether two node
// color values are similar or not
#define BEZDAT_SIM_TOLERANCE_COL Real(32)

// Multiple of the RGB8 color space unit for use as a tolerance to decide whether two
// node color derivatives are similar or not
#define BEZDAT_SIM_TOLERANCE_DCOL Real(0.0078125)

	// One point in a bezdat dataset
	template <class Real>
	struct BezdatPoint
{
	typedef typename HermiteSpline<Real>::Vec3 Vec3;
	Vec3 pos, color;
	Real radius;
};

// One segment in a bezdat dataset
struct BezdatSegment
{
	unsigned idx[4];
};

// One Hermite position node directly converted from a bezdat dataset
template <class Real>
struct BezdatNode
{
	// Types
	typedef typename BezdatPoint<Real>::Vec3 Vec3;
	typedef typename HermiteSpline<Real>::Vec4 Vec4;

	// Data members
	Vec3 pos, dpos, col, dcol;
	Real rad, drad;

	// Methods
	BezdatNode() {};
	BezdatNode(unsigned iPnt, unsigned iAux, const BezdatSegment &seg,
	           const std::vector<BezdatPoint<Real> > &points)
	{
		pos = points[seg.idx[iPnt]].pos;
		col = points[seg.idx[iPnt]].color / Real(255);
		rad = points[seg.idx[iPnt]].radius;
		if (iAux & 0x1)
		{
			dpos = (points[seg.idx[iAux]].pos-points[seg.idx[iPnt]].pos) * Real(3);
			dcol =   (points[seg.idx[iAux]].color-points[seg.idx[iPnt]].color) * Real(3)
			       / Real(255);
			drad = (points[seg.idx[iAux]].radius-points[seg.idx[iPnt]].radius) * Real(3);
		}
		else
		{
			dpos = (points[seg.idx[iPnt]].pos-points[seg.idx[iAux]].pos) * Real(3);
			dcol =   (points[seg.idx[iPnt]].color-points[seg.idx[iAux]].color) * Real(3)
			       / Real(255);
			drad = (points[seg.idx[iPnt]].radius-points[seg.idx[iAux]].radius) * Real(3);
		}
	}

	bool isSimilar (
		const HermiteNode<Vec4> &position, const HermiteNode<Real> &radius,
		const HermiteNode<Vec4> &color, Real pUnit=1, Real rUnit=1, Real cUnit=1
	) const
	{
		// ToDo: actually parametrize value and angle tolerances
		const Real 
			diff_dpos=glm::distance2(dpos, position.der.xyz()),
			thr_dpos=BEZDAT_SIM_TOLERANCE_DPOS*BEZDAT_SIM_TOLERANCE_DPOS * pUnit*pUnit,
			diff_drad=glm::abs(drad - radius.der),
			thr_drad=BEZDAT_SIM_TOLERANCE_DRAD * rUnit,
			diff_dcol=glm::distance2(dcol, color.der.xyz()),
			thr_dcol=BEZDAT_SIM_TOLERANCE_DCOL*BEZDAT_SIM_TOLERANCE_DPOS * cUnit*cUnit;

		return   (   glm::distance2(pos, position.val.xyz())
		          <= BEZDAT_SIM_TOLERANCE_POS*std::numeric_limits<Real>::epsilon())
		      && (   glm::abs(rad - radius.val)
		          <= BEZDAT_SIM_TOLERANCE_RAD*std::numeric_limits<Real>::epsilon())
		      && (   glm::distance2(col, color.val.xyz())
		          <= BEZDAT_SIM_TOLERANCE_COL*std::numeric_limits<Real>::epsilon())
		      && (diff_dpos<=thr_dpos) && (diff_drad<=thr_drad) && (diff_dcol<=thr_dcol);
	}
};

// Anonymous namespace end
}



//////
//
// Class implementations
//

////
// BezdatHandler

struct BezdatHandler_impl
{
	// It seems we don't actually need any state at all?
};
#define BEZDHANDLER_IMPL ((BezdatHandler_impl*)pimpl)

template <class FltType>
BezdatHandler<FltType>::BezdatHandler()
	: pimpl(nullptr)
{
	pimpl = new BezdatHandler_impl;
}

template <class FltType>
BezdatHandler<FltType>::~BezdatHandler()
{
	if (pimpl)
		delete BEZDHANDLER_IMPL;
}

template<>
const BezdatHandler<float>& BezdatHandler<float>::obtainRef (void)
{
	static BezdatHandler<float> theBezdatHandlerFlt; // <-- thread-safe since C++11
	return theBezdatHandlerFlt;
}
template<>
const BezdatHandler<double>& BezdatHandler<double>::obtainRef(void)
{
	static BezdatHandler<double> theBezdatHandlerDbl; // <-- thread-safe since C++11
	return theBezdatHandlerDbl;
}

template <class FltType>
const BezdatHandler<FltType>* BezdatHandler<FltType>::obtainPtr (void)
{
	return &(obtainRef());
}

template <class FltType>
HermiteSpline<FltType> BezdatHandler<FltType>::read (std::istream &contents) const
{
	// Declare result structs
	HermiteSpline<Real> spline;

	// Check stream for compatibility
	std::string str(1023, 0);
	contents.getline(const_cast<char*>(str.c_str()), 1023);
	if (strCmpI(str.c_str(), "BezDatA 1.0"))
		throw cgvu::err::NotSupportedException(CGVU_LOGMSG(
			"BezdatHandler::read", "First line in stream must be \"BezDatA 1.0\", but"
			" found" + str + "instead!"
		));


	////
	// Read bezdat contents

	// Bezdat database
	std::vector<BezdatPoint<Real> > points;
	std::vector<BezdatSegment> segments;
	Real avgNodeDistance=0, avgRadiusDiff=0, avgColorDiff=0;

	// Parse the stream until EOF
	contents >> str;
	while (!contents.eof())
	{
		if (strCmpI(str.c_str(), "PT") == 0)
		{
			BezdatPoint<Real> &newPoint = points.emplace_back();
			cgvu::util::streamToVec(&newPoint.pos, contents);
			contents >> newPoint.radius;
			cgvu::util::streamToVec(&newPoint.color, contents);
		}
		else if (strCmpI(str.c_str(), "BC") == 0)
		{
			// Read the segment
			BezdatSegment &newSegment = segments.emplace_back();
			cgvu::util::streamToVec(&newSegment.idx, contents, 4);
			// Update estimate of average per-attribute node distances
			Real curNodeDistance = glm::distance(points[newSegment.idx[0]].pos,
			                                     points[newSegment.idx[3]].pos),
			     curRadiusDiff = glm::abs(  points[newSegment.idx[3]].radius
			                              - points[newSegment.idx[0]].radius),
			     curColorDiff = glm::distance(points[newSegment.idx[0]].color,
			                                  points[newSegment.idx[3]].color);
			const size_t num = segments.size();
			avgNodeDistance =
				(Real(num-1)*avgNodeDistance + curNodeDistance) / Real(num);
			avgRadiusDiff =
				(Real(num-1)*avgRadiusDiff + curRadiusDiff) / Real(num);
			avgColorDiff =
				(Real(num-1)*avgColorDiff + curColorDiff) / Real(num);
		}
		contents >> str;
	}


	////
	// Convert to Hermite spline format

	// Build database of Hermite node data
	cgvu::util::Grid3D<Real> nodesDB(avgNodeDistance*Real(0.015625),
		[&spline] (Vec3 *pnt, size_t id) -> bool {
			*pnt = spline.positions[id].val.xyz();
			return true;
	});
	std::vector<std::vector<BezdatNode<Real> > > mergeLog;
	mergeLog.reserve(points.size());
	for (const auto &seg : segments)
	{
		// Add a new Hermite segment
		auto &newSeg = spline.segments.emplace_back();

		// Unique-ify this control point combination
		for (unsigned i=0; i<2; i++)
		{
			// Determine which is outer and which is inner control point
			const unsigned iPnt=i*2+i, iAux=i*2+1-i;
			const BezdatPoint<Real> &pnt = points[seg.idx[iPnt]];

			// Build list of nodes close to the one we're about to construct
			std::vector<size_t> neighbors;
			nodesDB.query(&neighbors, pnt.pos, true);

			// Check if we can reuse a node
			bool notFound = true;
			BezdatNode<Real> newNode(iPnt, iAux, seg, points);
			for (size_t nbId : neighbors)
			{
				if (newNode.isSimilar(
					spline.positions[nbId], spline.radii[nbId], spline.colors[nbId],
					avgNodeDistance, avgRadiusDiff, avgColorDiff
				))
				{
					// Node already exists, log the duplicate and reference it
					mergeLog[nbId].emplace_back(std::move(newNode));
					newSeg.n[i] = (unsigned)nbId;
					notFound = false;
					break;
				}
			}
			if (notFound)
			{
				// This node is not yet in the database
				// - determine index of new node
				unsigned id = (unsigned)spline.positions.size();
				// - create new node
				HermiteNode<Vec4> &newPos = spline.positions.emplace_back();
				HermiteNode<Real> &newRad = spline.radii.emplace_back();
				HermiteNode<Vec4> &newCol = spline.colors.emplace_back();
				mergeLog.emplace_back();
				// - set values and tangents of new node
				newPos.val = Vec4(newNode.pos, 1);
				newPos.der = Vec4(newNode.dpos, 0);
				newRad.val = newNode.rad;
				newRad.der = newNode.drad;
				newCol.val = Vec4(newNode.col, 1);
				newCol.der = Vec4(newNode.dcol, 0);
				// - reference the new node
				nodesDB.insert(id);
				newSeg.n[i] = id;
				// - Check if this looks like a new tube (indicated by discontinuity at
				//   first node of the segment)
				if (i==0)
					spline.curveCount++;
			}
		}
	}

	// Move merged nodes to position of cluster average
	for (unsigned i=0; i<spline.positions.size(); i++) if (!(mergeLog[i].empty()))
	{
		Vec3 posSum=spline.positions[i].val.xyz(), dposSum=spline.positions[i].der.xyz(),
		     colSum=spline.colors[i].val.xyz(), dcolSum=spline.colors[i].der.xyz();
		for (const auto &mn : mergeLog[i])
		{
			posSum += mn.pos;
			dposSum += mn.dpos;
			colSum += mn.col;
			dcolSum += mn.dcol;
			spline.radii[i].val += mn.rad;
			spline.radii[i].der += mn.drad;
		}
		Real num = Real(mergeLog[i].size()+1);
		spline.positions[i].val = Vec4(posSum/num, 1);
		spline.positions[i].der = Vec4(dposSum/num, 0);
		spline.colors[i].val = Vec4(colSum/num, 1);
		spline.colors[i].der = Vec4(dcolSum/num, 0);
		spline.radii[i].val /= num;
		spline.radii[i].der /= num;
	}


	// Done!
	unsigned in0=0, in1=1;
	return std::move(spline);
}

template <class FltType>
void BezdatHandler<FltType>::write (
	std::ostream &contents, const HermiteSpline<Real> &spline
) const
{
}



//////
//
// Explicit template instantiations
//

// Only float and double variants are intended
template class BezdatHandler<float>;
template class BezdatHandler<double>;
