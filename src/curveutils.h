
#ifndef __CURVEUTILS_H__
#define __CURVEUTILS_H__


//////
//
// Includes
//

// C++ STL
#include <limits>

// GLM library
#define GLM_FORCE_SWIZZLE 1
#include <glm/glm.hpp>



//////
//
// Global constants
//

/** @brief 1 over 3. */
template <class Real>
constexpr Real _1o3 = Real(1./3.);
/** @brief 2 over 3. */
template <class Real>
constexpr Real _2o3 = Real(2./3.);
/** @brief Positive infinity. */
template <class Real>
constexpr Real _posInf = std::numeric_limits<Real>::infinity();
/** @brief Negative infinity. */
template <class Real>
constexpr Real _negInf = -std::numeric_limits<Real>::infinity();

/** @brief Zero vector. */
template <class Real, unsigned dimension>
const glm::vec<dimension, Real> _zero{0};

/** @brief Vector of ones. */
template <class Real, unsigned dimension>
const glm::vec<dimension, Real> _one{1};

/** @brief Vector of twos. */
template <class Real, unsigned dimension>
const glm::vec<dimension, Real> _two{2};

/** @brief Boolean true vector. */
template <unsigned dimension>
const glm::vec<dimension, bool> _true{true};

/** @brief Cubic Hermite to Bezier basis transform. */
template <class Real>
const glm::mat<4, 4, Real> h2b = {
	1,          0,           0,  0,
	1, _1o3<Real>,           0,  0,
	0,          0, -_1o3<Real>,  1,
	0,          0,           0,  1
};
/** @brief Cubic Bezier to Hermite basis transform. */
template <class Real>
const glm::mat<4, 4, Real> b2h = {
	 1,  0,  0,  0,
	-3,  3,  0,  0,
	 0,  0, -3,  3,
	 0,  0,  0,  1
};

/** @brief Cubic Bezier to monomial basis transform. */
template <class Real>
const glm::mat<4, 4, Real> b2m = {
	-1,  3, -3,  1,
	 3, -6,  3,  0,
	-3,  3,  0,  0,
	 1,  0,  0,  0
};
/** @brief Cubic Monomial to Bezier basis transform. */
template <class Real>
const glm::mat<4, 4, Real> m2b = {
	0,          0,          0, 1,
	0,          0, _1o3<Real>, 1,
	0, _1o3<Real>, _2o3<Real>, 1,
	1,          1,          1, 1
};

/** @brief Quadratic Bezier to monomial basis transform. */
template <class Real>
const glm::mat<3, 3, Real> b2m_2 = {
	 1, -2,  1,
	-2,  2,  0,
	 1,  0,  0
};
/** @brief Quadratic Monomial to Bezier basis transform. */
template <class Real>
const glm::mat<3, 3, Real> m2b_2 = {
	0,  0,   1,
	0,  0.5, 1,
	1,  1,   1
};

/** @brief Linear Bezier to monomial basis transform. */
template <class Real>
const glm::mat<2, 2, Real> b2m_1 = {
	-1, 1,
	 1, 0
};
/** @brief Linear Monomial to Bezier basis transform. */
template <class Real>
const glm::mat<2, 2, Real> m2b_1 = {
	0, 1,
	1, 1
};



//////
//
// Typedefs
//

/** @brief Vector-valued cubic curve type. */
template<class Real, unsigned dimension>
using CubicCurve = glm::mat<4, dimension, Real>;

/** @brief 4D vector-valued cubic curve type. */
template<class Real>
using CubicCurve4 = CubicCurve<Real, 4>;

/** @brief 3D vector-valued cubic curve type. */
template<class Real>
using CubicCurve3 = CubicCurve<Real, 3>;

/** @brief 2D vector-valued cubic curve type. */
template<class Real>
using CubicCurve2 = CubicCurve<Real, 2>;

/** @brief Scalar-valued cubic curve type. */
template<class Real>
using CubicCurve1 = glm::vec<4, Real>;

/** @brief Vector-valued quadratic curve type. */
template<class Real, unsigned dimension>
using QuadraticCurve = glm::mat<3, dimension, Real>;

/** @brief 4D vector-valued quadratic curve type. */
template<class Real>
using QuadraticCurve4 = QuadraticCurve<Real, 4>;

/** @brief 3D vector-valued quadratic curve type. */
template<class Real>
using QuadraticCurve3 = QuadraticCurve<Real, 3>;

/** @brief 2D vector-valued quadratic curve type. */
template<class Real>
using QuadraticCurve2 = QuadraticCurve<Real, 2>;

/** @brief Scalar-valued quadratic curve type. */
template<class Real>
using QuadraticCurve1 = glm::vec<3, Real>;

/** @brief Vector-valued linear curve type. */
template<class Real, unsigned dimension>
using LinearCurve = glm::mat<2, dimension, Real>;

/** @brief 4D vector-valued linear curve type. */
template<class Real>
using LinearCurve4 = LinearCurve<Real, 4>;

/** @brief 3D vector-valued linear curve type. */
template<class Real>
using LinearCurve3 = LinearCurve<Real, 3>;

/** @brief 2D vector-valued linear curve type. */
template<class Real>
using LinearCurve2 = LinearCurve<Real, 2>;

/** @brief Scalar-valued linear curve type. */
template<class Real>
using LinearCurve1 = glm::vec<2, Real>;



//////
//
// Functions
//

////
// Component function extraction

/**
 * @brief
 *		Extracts the i-th component function from the given 4D vector-valued cubic curve
 */
template <class Real>
CubicCurve1<Real> getComponentFkt (const CubicCurve4<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i], curve[3][i] };
}

/**
 * @brief
 *		Extracts the i-th component function from the given 3D vector-valued cubic curve
 */
template <class Real>
CubicCurve1<Real> getComponentFkt (const CubicCurve3<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i], curve[3][i] };
}

/**
 * @brief
 *		Extracts the i-th component function from the given 2D vector-valued cubic curve
 */
template <class Real>
CubicCurve1<Real> getComponentFkt (const CubicCurve2<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i], curve[3][i] };
}

/**
 * @brief
 *		Extracts the i-th component function from the given 4D vector-valued quadratic
 *		curve
 */
template <class Real>
QuadraticCurve1<Real> getComponentFkt(const QuadraticCurve4<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i] };
}

/**
 * @brief
 *		Extracts the i-th component function from the given 3D vector-valued quadratic
 *		curve
 */
template <class Real>
QuadraticCurve1<Real> getComponentFkt(const QuadraticCurve3<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i] };
}

/**
 * @brief
 *		Extracts the i-th component function from the given 2D vector-valued quadratic
 *		curve
 */
template <class Real>
QuadraticCurve1<Real> getComponentFkt(const QuadraticCurve2<Real> &curve, int i)
{
	return { curve[0][i], curve[1][i], curve[2][i] };
}


////
// Bezier to Hermite conversion

/**
 * @brief
 *		Converts a 4D vector-valued cubic Hermite curve defined by the given control
 *		points to a cubic Bezier curve.
 */
template <class Real>
CubicCurve4<Real> toBezier (
	const glm::vec<4,Real> &n0, const glm::vec<4,Real> &t0,
	const glm::vec<4,Real> &n1, const glm::vec<4,Real> &t1
)
{
	return { n0, n0 + _1o3<Real>*t0, n1 - _1o3<Real>*t1, n1 };
}

/**
 * @brief
 *		Converts a 3D vector-valued cubic Hermite curve defined by the given control
 *		points to a cubic Bezier curve.
 */
template <class Real>
CubicCurve3<Real> toBezier(
	const glm::vec<3, Real> &n0, const glm::vec<3, Real> &t0,
	const glm::vec<3, Real> &n1, const glm::vec<3, Real> &t1)
{
	return { n0, n0 + _1o3<Real>*t0, n1 - _1o3<Real>*t1, n1 };
}

/**
 * @brief
 *		Converts a 2D vector-valued cubic Hermite curve defined by the given control
 *		points to a cubic Bezier curve.
 */
template <class Real>
CubicCurve2<Real> toBezier (
	const glm::vec<2,Real> &n0, const glm::vec<2,Real> &t0,
	const glm::vec<2,Real> &n1, const glm::vec<2,Real> &t1
)
{
	return { n0, n0 + _1o3<Real>*t0, n1 - _1o3<Real>*t1, n1 };
}

/**
 * @brief
 *		Converts a scalar-valued cubic Hermite curve defined by the given control points
 *		to a cubic Bezier curve.
 */
template <class Real>
CubicCurve1<Real> toBezier (Real n0, Real t0, Real n1, Real t1)
{
	return { n0, n0 + _1o3<Real>*t0, n1 - _1o3<Real>*t1, n1 };
}

/**
 * @brief
 *		Converts a vector-valued cubic Hermite curve to a cubic Bezier curve, writing the
 *		control points to the output parameter.
 */
template <class CurveType>
void toBezier (
	CurveType &b,
	const typename CurveType::col_type &n0, const typename CurveType::col_type &t0,
	const typename CurveType::col_type &n1, const typename CurveType::col_type &t1
)
{
	b[0] = n0;
	b[1] = n0 + _1o3<typename CurveType::value_type>*t0;
	b[2] = n1 - _1o3<typename CurveType::value_type>*t1;
	b[3] = n1;
}

/**
 * @brief
 *		Converts a scalar-valued cubic Hermite curve to a cubic Bezier curve, writing the
 *		control points to the output parameter
 */
template <class Real>
void toBezier (CubicCurve1<Real> &b, Real n0, Real t0, Real n1, Real t1)
{
	b[0] = n0;
	b[1] = n0 + _1o3<Real>*t0;
	b[2] = n1 - _1o3<Real>*t1;
	b[3] = n1;
}

/** @brief Converts a cubic Hermite curve to a cubic Bezier curve. */
template <class CurveType>
CurveType toBezier (const CurveType &h)
{
	return h * h2b<typename CurveType::value_type>;
}

/**
 * @brief
 *		Converts a cubic Hermite curve to a cubic Bezier curve, writing the control
 *		points to the output parameter.
 */
template <class CurveType>
void toBezier (CurveType &b, const CurveType &h)
{
	b = h * h2b<typename CurveType::value_type>;
}


////
// Bezier to Hermite conversion

/**
 * @brief
 *		Converts a vector-valued cubic Bezier curve defined by the given control points
 *		to a cubic Hermite curve.
 */
template <class Real, unsigned dimension>
CubicCurve<Real, dimension> toHermite (
	const glm::vec<dimension,Real> &b0, const glm::vec<dimension,Real> &b1,
	const glm::vec<dimension,Real> &b2, const glm::vec<dimension,Real> &b3
)
{
	return { b0, Real(3)*(b1-b0), Real(3)*(b3-b2), b3 };
}

/**
 * @brief
 *		Converts a scalar-valued cubic Bezier curve defined by the given control points
 *		to a cubic Hermite curve.
 */
template <class Real>
CubicCurve1<Real> toHermite (Real b0, Real b1, Real b2, Real b3)
{
	return { b0, Real(3)*(b1 - b0), Real(3)*(b3 - b2), b3 };
}

/**
 * @brief
 *		Converts a vector-valued cubic Bezier curve to a cubic Hermite curve, writing the
 *		control points to the output parameter.
 */
template <class CurveType>
void toHermite (
	CurveType &h,
	const typename CurveType::col_type &b0, const typename CurveType::col_type &b1,
	const typename CurveType::col_type &b2, const typename CurveType::col_type &b3
)
{
	h[0] = b0;
	h[1] = typename CurveType::value_type(3)*(b1-b0);
	h[2] = typename CurveType::value_type(3)*(b3-b2);
	h[3] = b3;
}

/**
 * @brief
 *		Converts a scalar-valued cubic Bezier curve to a cubic Hermite curve, writing the
 *		control points to the output parameter
 */
template <class Real>
void toHermite (CubicCurve1<Real> &h, Real b0, Real b1, Real b2, Real b3)
{
	h[0] = b0;
	h[1] = Real(3)*(b1-b0);
	h[2] = Real(3)*(b3-b2);
	h[3] = b3;
}

/** @brief Converts a cubic Bezier curve to a cubic Hermite curve. */
template <class CurveType>
CurveType toHermite (const CurveType &b)
{
	return b * b2h<CurveType::value_type>;
}

/**
 * @brief
 *		Converts a cubic Bezier curve to a cubic Hermite curve, writing the control
 *		points to the output parameter.
 */
template <class CurveType>
void toHermite (CurveType &h, const CurveType &b)
{
	h = b * b2h<CurveType::value_type>;
}


////
// Cubic Bezier to Monomial conversion

/**
 * @brief
 *		Converts a 4D vector-valued cubic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
CubicCurve4<Real> toMonomial (
	const glm::vec<4,Real> &b0, const glm::vec<4,Real> &b1,
	const glm::vec<4,Real> &b2, const glm::vec<4,Real> &b3
)
{
	return {         -b0 + Real(3)*b1 - Real(3)*b2 + b3,
	          Real(3)*b0 - Real(6)*b1 + Real(3)*b2,
	         Real(-3)*b0 + Real(3)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a 3D vector-valued cubic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
CubicCurve3<Real> toMonomial (
	const glm::vec<3,Real> &b0, const glm::vec<3,Real> &b1,
	const glm::vec<3,Real> &b2, const glm::vec<3,Real> &b3
)
{
	return {         -b0 + Real(3)*b1 - Real(3)*b2 + b3,
	          Real(3)*b0 - Real(6)*b1 + Real(3)*b2,
	         Real(-3)*b0 + Real(3)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a 2D vector-valued cubic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
CubicCurve2<Real> toMonomial (
	const glm::vec<2,Real> &b0, const glm::vec<2,Real> &b1,
	const glm::vec<2,Real> &b2, const glm::vec<2,Real> &b3
)
{
	return {         -b0 + Real(3)*b1 - Real(3)*b2 + b3,
	          Real(3)*b0 - Real(6)*b1 + Real(3)*b2,
	         Real(-3)*b0 + Real(3)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a scalar-valued cubic Bezier curve defined by the given control points
 *		into canonical polynomial form.
 */
template <class Real>
CubicCurve1<Real> toMonomial (Real b0, Real b1, Real b2, Real b3)
{
	return {         -b0 + Real(3)*b1 - Real(3)*b2 + b3,
	          Real(3)*b0 - Real(6)*b1 + Real(3)*b2,
	         Real(-3)*b0 + Real(3)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a vector-valued cubic Bezier curve into canonical polynomial form,
 *		writing the coefficient points to the output parameter.
 */
template <class CurveType>
void toMonomial (
	CurveType &m,
	const typename CurveType::col_type &b0, const typename CurveType::col_type &b1,
	const typename CurveType::col_type &b2, const typename CurveType::col_type &b3
)
{
	typedef typename CurveType::value_type Real;
	m[0] =         -b0 + Real(3)*b1 - Real(3)*b2 + b3;
	m[1] =  Real(3)*b0 - Real(6)*b1 + Real(3)*b2;
	m[2] = Real(-3)*b0 + Real(3)*b1;
	m[3] =          b0;
}

/**
 * @brief
 *		Converts a scalar-valued cubic Bezier curve into canonical polynomial form,
 *		writing the polynomial coefficients to the output parameter.
 */
template <class Real>
void toMonomial (CubicCurve1<Real> &m, Real b0, Real b1, Real b2, Real b3)
{
	m[0] =         -b0 + Real(3)*b1 - Real(3)*b2 + b3;
	m[1] =  Real(3)*b0 - Real(6)*b1 + Real(3)*b2;
	m[2] = Real(-3)*b0 + Real(3)*b1;
	m[3] =          b0;
}

/** @brief Converts a 4D cubic Bezier curve into canonical polynomial form. */
template <class Real>
CubicCurve4<Real> toMonomial (const CubicCurve4<Real> &b)
{
	return b * b2m<Real>;
}

/** @brief Converts a 3D cubic Bezier curve into canonical polynomial form. */
template <class Real>
CubicCurve3<Real> toMonomial (const CubicCurve3<Real> &b)
{
	return b * b2m<Real>;
}

/** @brief Converts a 2D cubic Bezier curve into canonical polynomial form. */
template <class Real>
CubicCurve2<Real> toMonomial (const CubicCurve2<Real> &b)
{
	return b * b2m<Real>;
}

/** @brief Converts a scalar cubic Bezier curve into canonical polynomial form. */
template <class Real>
CubicCurve1<Real> toMonomial (const CubicCurve1<Real> &b)
{
	return b * b2m<Real>;
}

/**
 * @brief
 *		Converts a 4D cubic Bezier curve into canonical polynomial form, writing the
 *		control points to the output parameter.
 */
template <class Real>
void toMonomial (CubicCurve4<Real> &m, const CubicCurve4<Real> &b)
{
	m = b * b2m<Real>;
}

/**
 * @brief
 *		Converts a 3D cubic Bezier curve into canonical polynomial form, writing the
 *		control points to the output parameter.
 */
template <class Real>
void toMonomial (CubicCurve3<Real> &m, const CubicCurve3<Real> &b)
{
	m = b * b2m<Real>;
}

/**
 * @brief
 *		Converts a 2D cubic Bezier curve into canonical polynomial form, writing the
 *		control points to the output parameter.
 */
template <class Real>
void toMonomial (CubicCurve2<Real> &m, const CubicCurve2<Real> &b)
{
	m = b * b2m<Real>;
}

/**
 * @brief
 *		Converts a scalar cubic Bezier curve into canonical polynomial form, writing the
 *		control points to the output parameter.
 */
template <class Real>
void toMonomial (CubicCurve1<Real> &m, const CubicCurve1<Real> &b)
{
	m = b * b2m<Real>;
}


////
// Quadratic Bezier to Monomial conversion

/**
 * @brief
 *		Converts a 4D vector-valued quadratic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
QuadraticCurve4<Real> toMonomial (
	const glm::vec<4,Real> &b0, const glm::vec<4,Real> &b1, const glm::vec<4,Real> &b2
)
{
	return {          b0 - Real(2)*b1 + b2,
	         Real(-2)*b0 + Real(2)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a 3D vector-valued quadratic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
QuadraticCurve3<Real> toMonomial (
	const glm::vec<3,Real> &b0, const glm::vec<3,Real> &b1, const glm::vec<3,Real> &b2
)
{
	return {          b0 - Real(2)*b1 + b2,
	         Real(-2)*b0 + Real(2)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a 2D vector-valued quadratic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
QuadraticCurve2<Real> toMonomial (
	const glm::vec<2,Real> &b0, const glm::vec<2,Real> &b1, const glm::vec<2,Real> &b2
)
{
	return {          b0 - Real(2)*b1 + b2,
	         Real(-2)*b0 + Real(2)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a scalar-valued quadratic Bezier curve defined by the given control
 *		points into canonical polynomial form.
 */
template <class Real>
QuadraticCurve1<Real> toMonomial (Real b0, Real b1, Real b2)
{
	return {          b0 - Real(2)*b1 + b2,
	         Real(-2)*b0 + Real(2)*b1,
	                  b0 };
}

/**
 * @brief
 *		Converts a vector-valued quadratic Bezier curve into canonical polynomial form,
 *		writing the coefficient points to the output parameter.
 */
template <class CurveType>
void toMonomial (
	CurveType &m, const typename CurveType::col_type &b0,
	const typename CurveType::col_type &b1, const typename CurveType::col_type &b2
)
{
	typedef typename CurveType::value_type Real;
	m[0] =          b0 - Real(2)*b1 + b2;
	m[1] = Real(-2)*b0 + Real(2)*b1;
	m[2] =          b0;
}

/**
 * @brief
 *		Converts a scalar-valued quadratic Bezier curve into canonical polynomial form,
 *		writing the polynomial coefficients to the output parameter.
 */
template <class Real>
void toMonomial (QuadraticCurve1<Real> &m, Real b0, Real b1, Real b2)
{
	m[0] =          b0 - Real(2)*b1 + b2;
	m[1] = Real(-2)*b0 + Real(2)*b1;
	m[2] =          b0;
}

/**
 * @brief
 *		Converts a 4D vector-valued quadratic Bezier curve into canonical polynomial
 *		form.
 */
template <class Real>
QuadraticCurve4<Real> toMonomial (const QuadraticCurve4<Real> &b)
{
	return b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a 3D vector-valued quadratic Bezier curve into canonical polynomial
 *		form.
 */
template <class Real>
QuadraticCurve3<Real> toMonomial (const QuadraticCurve3<Real> &b)
{
	return b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a 2D vector-valued quadratic Bezier curve into canonical polynomial
 *		form.
 */
template <class Real>
QuadraticCurve2<Real> toMonomial (const QuadraticCurve2<Real> &b)
{
	return b * b2m_2<Real>;
}

/**
 * @brief Converts a scalar-valued quadratic Bezier curve into canonical polynomial form.
 */
template <class Real>
QuadraticCurve1<Real> toMonomial (const QuadraticCurve1<Real> &b)
{
	return b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a 4D vector-valued quadratic Bezier curve into canonical polynomial
 *		form, writing the coefficient points to the output parameter.
 */
template <class Real>
void toMonomial (QuadraticCurve4<Real> &m, const QuadraticCurve4<Real> &b)
{
	m = b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a 3D vector-valued quadratic Bezier curve into canonical polynomial
 *		form, writing the coefficient points to the output parameter.
 */
template <class Real>
void toMonomial (QuadraticCurve3<Real> &m, const QuadraticCurve3<Real> &b)
{
	m = b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a 2D vector-valued quadratic Bezier curve into canonical polynomial
 *		form, writing the coefficient points to the output parameter.
 */
template <class Real>
void toMonomial (QuadraticCurve2<Real> &m, const QuadraticCurve2<Real> &b)
{
	m = b * b2m_2<Real>;
}

/**
 * @brief
 *		Converts a scalar-valued quadratic Bezier curve into canonical polynomial form,
 *		writing the polynomial coefficients to the output parameter.
 */
template <class Real>
void toMonomial (QuadraticCurve1<Real> &m, const QuadraticCurve1<Real> &b)
{
	m = b * b2m_2<Real>;
}


////
// Bezier curve derivatives

/**
 * @brief
 *		Derives the given 4D vector-valued cubic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
QuadraticCurve4<Real> deriveBezier (const CubicCurve4<Real> &b)
{
	return { Real(3)*(b[1]-b[0]), Real(3)*(b[2]-b[1]), Real(3)*(b[3]-b[2]) };
}

/**
 * @brief
 *		Derives the given 3D vector-valued cubic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
QuadraticCurve3<Real> deriveBezier (const CubicCurve3<Real> &b)
{
	return { Real(3)*(b[1]-b[0]), Real(3)*(b[2]-b[1]), Real(3)*(b[3]-b[2]) };
}

/**
 * @brief
 *		Derives the given 2D vector-valued cubic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
QuadraticCurve2<Real> deriveBezier (const CubicCurve2<Real> &b)
{
	return { Real(3)*(b[1]-b[0]), Real(3)*(b[2]-b[1]), Real(3)*(b[3]-b[2]) };
}

/**
 * @brief
 *		Derives the given scalar-valued cubic Bezier curve, returning the control points
 *		of the resulting quadratic Bezier curve
 */
template <class Real>
QuadraticCurve1<Real> deriveBezier (const CubicCurve1<Real> &b)
{
	return { Real(3)*(b[1]-b[0]), Real(3)*(b[2]-b[1]), Real(3)*(b[3]-b[2]) };
}

/**
 * @brief
 *		Derives the given 4D vector-valued quadratic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
LinearCurve4<Real> deriveBezier (const QuadraticCurve4<Real> &b)
{
	return { Real(2)*(b[1]-b[0]), Real(2)*(b[2]-b[1]) };
}

/**
 * @brief
 *		Derives the given 3D vector-valued quadratic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
LinearCurve3<Real> deriveBezier (const QuadraticCurve3<Real> &b)
{
	return { Real(2)*(b[1]-b[0]), Real(2)*(b[2]-b[1]) };
}

/**
 * @brief
 *		Derives the given 2D vector-valued quadratic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
LinearCurve2<Real> deriveBezier (const QuadraticCurve2<Real> &b)
{
	return { Real(2)*(b[1]-b[0]), Real(2)*(b[2]-b[1]) };
}

/**
 * @brief
 *		Derives the given scalar-valued quadratic Bezier curve, returning the control
 *		points of the resulting quadratic Bezier curve
 */
template <class Real>
LinearCurve1<Real> deriveBezier (const QuadraticCurve1<Real> &b)
{
	return { Real(2)*(b[1]-b[0]), Real(2)*(b[2]-b[1]) };
}


////
// Bezier curve evaluation

/** @brief Evaluates the given 4D vector-valued cubic Bezier curve at a given t=0..1. */
template <class Real>
glm::vec<4,Real> evalBezier (const CubicCurve4<Real> &b, Real t)
{
	glm::mat<3, 4, Real> v = {
		glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t), glm::mix(b[2], b[3], t)
	};
	v[0] = glm::mix(v[0], v[1], t);
	v[1] = glm::mix(v[1], v[2], t);
	return glm::mix(v[0], v[1], t);
}

/** @brief Evaluates the given 3D vector-valued cubic Bezier curve at a given t=0..1. */
template <class Real>
glm::vec<3, Real> evalBezier (const CubicCurve3<Real> &b, Real t)
{
	glm::mat<3, 3, Real> v = {
		glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t), glm::mix(b[2], b[3], t)};
	v[0] = glm::mix(v[0], v[1], t);
	v[1] = glm::mix(v[1], v[2], t);
	return glm::mix(v[0], v[1], t);
}

/** @brief Evaluates the given 2D vector-valued cubic Bezier curve at a given t=0..1. */
template <class Real>
glm::vec<2,Real> evalBezier (const CubicCurve2<Real> &b, Real t)
{
	glm::mat<3, 2, Real> v = {
		glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t), glm::mix(b[2], b[3], t)
	};
	v[0] = glm::mix(v[0], v[1], t);
	v[1] = glm::mix(v[1], v[2], t);
	return glm::mix(v[0], v[1], t);
}

/** @brief Evaluates the given scalar-valued cubic Bezier curve at a given t=0..1. */
template <class Real>
Real evalBezier (const CubicCurve1<Real> &b, Real t)
{
	glm::vec<3, Real> v = {
		glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t), glm::mix(b[2], b[3], t)
	};
	v[0] = glm::mix(v[0], v[1], t);
	v[1] = glm::mix(v[1], v[2], t);
	return glm::mix(v[0], v[1], t);
}

/**
 * @brief Evaluates the given 4D vector-valued quadratic Bezier curve at a given t=0..1.
 */
template <class Real>
glm::vec<4,Real> evalBezier (const QuadraticCurve4<Real> &b, Real t)
{
	const glm::mat<2, 4, Real> v = { glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t) };
	return glm::mix(v[0], v[1], t);
}

/**
 * @brief Evaluates the given 3D vector-valued quadratic Bezier curve at a given t=0..1.
 */
template <class Real>
glm::vec<3,Real> evalBezier (const QuadraticCurve3<Real> &b, Real t)
{
	const glm::mat<2, 3, Real> v = { glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t) };
	return glm::mix(v[0], v[1], t);
}

/**
 * @brief Evaluates the given 2D vector-valued quadratic Bezier curve at a given t=0..1.
 */
template <class Real>
glm::vec<2,Real> evalBezier (const QuadraticCurve2<Real> &b, Real t)
{
	const glm::mat<2, 2, Real> v = { glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t) };
	return glm::mix(v[0], v[1], t);
}

/** @brief Evaluates the given scalar-valued quadratic Bezier curve at a given t=0..1. */
template <class Real>
Real evalBezier (const QuadraticCurve1<Real> &b, Real t)
{
	const glm::vec<2, Real> v = { glm::mix(b[0], b[1], t), glm::mix(b[1], b[2], t) };
	return glm::mix(v[0], v[1], t);
}


////
// Monomial curve evaluation

/**
 * @brief Evaluates the given 4D vector-valued cubic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<4,Real> evalMonomial (const CubicCurve4<Real> &m, Real t)
{
	return m[3] + t*(m[2] + t*(m[1] + t*m[0]));
}

/**
 * @brief Evaluates the given 3D vector-valued cubic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<3, Real> evalMonomial (const CubicCurve3<Real> &m, Real t)
{
	return m[3] + t*(m[2] + t*(m[1] + t*m[0]));
}

/**
 * @brief Evaluates the given 2D vector-valued cubic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<2,Real> evalMonomial (const CubicCurve2<Real> &m, Real t)
{
	return m[3] + t*(m[2] + t*(m[1] + t*m[0]));
}

/** @brief Evaluates the given scalar -valued cubic monomial curve at a given t=0..1. */
template <class Real>
Real evalMonomial (const CubicCurve1<Real> &m, Real t)
{
	return m[3] + t*(m[2] + t*(m[1] + t*m[0]));
}

/**
 * @brief
 *		Evaluates the given 4D vector-valued quadratic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<4,Real> evalMonomial (const QuadraticCurve4<Real> &m, Real t)
{
	return m[2] + t*(m[1] + t*m[0]);
}

/**
 * @brief
 *		Evaluates the given 3D vector-valued quadratic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<3,Real> evalMonomial (const QuadraticCurve3<Real> &m, Real t)
{
	return m[2] + t*(m[1] + t*m[0]);
}

/**
 * @brief
 *		Evaluates the given 2D vector-valued quadratic monomial curve at a given t=0..1.
 */
template <class Real>
glm::vec<2,Real> evalMonomial (const QuadraticCurve2<Real> &m, Real t)
{
	return m[2] + t*(m[1] + t*m[0]);
}

/**
 * @brief Evaluates the given scalar-valued quadratic monomial curve at a given t=0..1.
 */
template <class Real>
Real evalMonomial (const QuadraticCurve1<Real> &m, Real t)
{
	return m[2] + t*(m[1] + t*m[0]);
}


////
// Quadratic solvers

/**
 * @brief
 *		Solves the quadratic formula for the given combination of precalculated terms,
 *		returning the number of real solutions in the first element of the result vector.
 */
template <class Real>
glm::vec<3, Real> solveQuadratic (Real _negb, Real _2a, Real D, Real _2c)
{
	if (_2a != 0)
	{
		// Quadratic
		if (D > 0)
		{
			const Real sqrtD = sqrt(D);
			return { 2, (_negb-sqrtD)/_2a, (_negb+sqrtD)/_2a };
		}
		else if (D == 0)
			return { 1, _negb/_2a, 0 };
	}
	else if (_negb != 0)
		// Linear
		return { 1, _2c/(2*_negb), 0 };

	// No solution
	return glm::vec<3, Real>(0);
}

/**
 * @brief
 *		Solves the quadratic formula for the given 4D vector-valued monomial curve,
 *		returning the number of real solutions per dimension in the first column of the
 *		result matrix.
 */
template <class Real>
glm::mat<3, 4, Real> solveQuadratic(const QuadraticCurve4<Real> &m)
{
	// Result matrix
	glm::mat<3, 4, Real> result;

	// Common terms and discriminant
	const glm::vec<4,Real> _negb = -m[1], _2a = Real(2)*m[0], _2c = Real(2)*m[2],
	                       D = m[1]*m[1] - _2a*_2c;
	if (   glm::all(glm::greaterThan(D, _zero<Real,4>))
	    && glm::all(glm::notEqual(_2a, _zero<Real,4>)))
	{
		// Full SIMD ahead
		const glm::vec<4,Real> sqrtD = sqrt(D);
		result[0] = _two<Real,4>;
		result[1] = (_negb - sqrtD) / _2a;
		result[2] = (_negb + sqrtD) / _2a;
		return result;
	}
	else
	{
		// Solve each component polynomial individually
		for (int i=0; i<4; i++)
		{
			const glm::vec<3,Real> cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
			result[0][i] = cres[0];
			result[1][i] = cres[1];
			result[2][i] = cres[2];
		}
		return result;
	}
}

/**
 * @brief
 *		Solves the quadratic formula for the given 3D vector-valued monomial curve,
 *		returning the number of real solutions per dimension in the first column of the
 *		result matrix.
 */
template <class Real>
glm::mat<3, 3, Real> solveQuadratic(const QuadraticCurve3<Real> &m)
{
	// Result matrix
	glm::mat<3, 3, Real> result;

	// Common terms and discriminant
	const glm::vec<3,Real> _negb = -m[1], _2a = Real(2)*m[0], _2c = Real(2)*m[2],
	                       D = m[1]*m[1] - _2a*_2c;
	if (   glm::all(glm::greaterThan(D, _zero<Real,3>))
	    && glm::all(glm::notEqual(_2a, _zero<Real,3>)))
	{
		// Full SIMD ahead
		const glm::vec<3,Real> sqrtD = sqrt(D);
		result[0] = _two<Real,3>;
		result[1] = (_negb - sqrtD) / _2a;
		result[2] = (_negb + sqrtD) / _2a;
		return result;
	}
	else
	{
		// Solve each component polynomial individually
		for (int i=0; i<3; i++)
		{
			const glm::vec<3,Real> cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
			result[0][i] = cres[0];
			result[1][i] = cres[1];
			result[2][i] = cres[2];
		}
		return result;
	}
}

/**
 * @brief
 *		Solves the quadratic formula for the given 2D vector-valued monomial curve,
 *		returning the number of real solutions per dimension in the first column of the
 *		result matrix.
 */
template <class Real>
glm::mat<3, 2, Real> solveQuadratic(const QuadraticCurve2<Real> &m)
{
	// Result matrix
	glm::mat<3, 2, Real> result;

	// Common terms and discriminant
	const glm::vec<2,Real> _negb = -m[1], _2a = Real(2)*m[0], _2c = Real(2)*m[2],
	                       D = m[1]*m[1] - _2a*_2c;
	if (   glm::all(glm::greaterThan(D, _zero<Real,2>))
	    && glm::all(glm::notEqual(_2a, _zero<Real,2>)))
	{
		// Full SIMD ahead
		const glm::vec<2,Real> sqrtD = sqrt(D);
		result[0] = _two<Real,2>;
		result[1] = (_negb - sqrtD) / _2a;
		result[2] = (_negb + sqrtD) / _2a;
		return result;
	}
	else
	{
		// Solve each component polynomial individually
		for (int i=0; i<2; i++)
		{
			const glm::vec<3,Real> cres = solveQuadratic(_negb[i], _2a[i], D[i], _2c[i]);
			result[0][i] = cres[0];
			result[1][i] = cres[1];
			result[2][i] = cres[2];
		}
		return result;
	}
}

/**
 * @brief
 *		Solves the quadratic formula for the given scalar-valued monomial curve,
 *		returning the number of real solutions in the first element of the result vector.
 */
template <class Real>
glm::vec<3, Real> solveQuadratic (const QuadraticCurve1<Real> &m)
{
	const Real _negb = -m[1], _2a = Real(2)*m[0], _2c = Real(2)*m[2],
	           D = m[1]*m[1] - _2a*_2c;
	return solveQuadratic(_negb, _2a, D, _2c);
}


////
// Miscellaneous

/** @brief Checks if the given parameter is inside a given interval. */
template <class Real>
bool isBetween (Real t, Real tmin, Real tmax)
{
	return t >= tmin && t <= tmax;
}

/** @brief Checks if the given parameter is between 0 and 1. */
template <class Real>
bool isBetween01 (Real t)
{
	return t >= 0 && t <= 1;
}

/** @brief Checks if the given parameter is outside a given interval. */
template <class Real>
bool isOutside (Real t, Real tmin, Real tmax)
{
	return t < tmin || t > tmax;
}

/** @brief Checks if the given parameter is outside of 0 and 1. */
template <class Real>
bool isOutside01 (Real t)
{
	return t < 0 || t > 1;
}

/**
 * @brief Runs a series of tests on the curve utility functions.
 *
 * @note Purely meant for singel-step debugging - does not report any test failures.
 */
void testCurveUtils (void);


#endif // ifndef __CURVEUTILS_H__
