#version 450
#extension GL_KHR_vulkan_glsl : enable


////
// Shader config and constants

// Configuration defines
#define NOOP_FRAGMENT_PROCESSING 0

// Uniforms
layout(binding = 1) uniform CGVuMaterial {
	vec4 albedo;
	float specularity;
	float shininess;
} cgvu_mat;

layout(binding = 2) uniform CGVuGlobalLight {
	vec3 pos;   // in eye space
	vec3 color;
} cgvu_light;

// Inputs
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec4 in_color;

// Outputs
layout(location = 0) out vec4 out_color;


////
// Functions

// Shader entry point
void main()
{
#if (NOOP_FRAGMENT_PROCESSING)
	discard;
#else
	// Final color from material
	vec4 color = in_color * cgvu_mat.albedo;

	// Lighting
	// - init
	vec3 normal = -normalize(cross(dFdx(in_pos), dFdy(in_pos)));
	vec3 dirToLight = normalize(cgvu_light.pos - in_pos);
	// - diffuse term (lambert)
	vec3 diffuse = color.rgb*cgvu_light.color*max(dot(dirToLight, normal), 0);
	// - specular term (blinn-phong)
	vec3 halfVec = normalize(normalize(-in_pos) + dirToLight);
	vec3 specular =
		  color.rgb*cgvu_mat.specularity*cgvu_light.color
		* pow(max(dot(halfVec, normal), 0), cgvu_mat.shininess);

	// Final color
	out_color = vec4(diffuse + specular, color.a);
#endif
}
